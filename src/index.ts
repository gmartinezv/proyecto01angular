import express, { Application} from 'express';

import indexRutas from './routes/indexRutas';
import juegosRutas from './routes/juegosRutas';




class Server {

    public app :  Application;

    constructor(){

     this.app = express(); 
     this.config();
     this.router();

    }

    config() : void{
        this.app.set("port", process.env.PORT || 3000);
    }

    router(): void {
        this.app.use('/', indexRutas);
        this.app.use('/api/juegos', juegosRutas);

    }

    start(): void {
        this.app.listen(this.app.get('port') , () => {
            console.log(" Servidor en puerto : ", this.app.get('port'));
            // cambio en el archivo
        } );
    }
}

const server = new Server();
server.start();
