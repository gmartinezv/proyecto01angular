"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const indexRutas_1 = __importDefault(require("./routes/indexRutas"));
const juegosRutas_1 = __importDefault(require("./routes/juegosRutas"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.router();
    }
    config() {
        this.app.set("port", process.env.PORT || 3000);
    }
    router() {
        this.app.use('/', indexRutas_1.default);
        this.app.use('/api/juegos', juegosRutas_1.default);
    }
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log(" Servidor en puerto : ", this.app.get('port'));
            // cambio en el archivo
        });
    }
}
const server = new Server();
server.start();
